
<!DOCTYPE html>
<html class="" lang="en">
<head prefix="og: http://ogp.me/ns#">
<meta charset="utf-8">
<link href="https://assets.gitlab-static.net" rel="dns-prefetch">
<link crossorigin="" href="https://assets.gitlab-static.net" rel="preconnect">
<link as="style" crossorigin="anonymous" href="https://assets.gitlab-static.net/assets/application-ecb95f242819f380f03b449d66b47aaac40425bad270129e8e5e6cb84da021f6.css" rel="preload">
<link as="style" crossorigin="anonymous" href="https://assets.gitlab-static.net/assets/highlight/themes/white-3a5ccf16b3cb943249b10b6fd8a260ac3c8a79ea432c44c3886d1d1ea9df4694.css" rel="preload">
<link crossorigin="" href="snowplow.trx.gitlab.net" rel="preconnect">

<meta content="IE=edge" http-equiv="X-UA-Compatible">
<script nonce="Vqh9ZOuKQQXxBS8LR2PXfA==">
//<![CDATA[
var gl = window.gl || {};
gl.startup_calls = {"/BotolBaba/bind/-/refs/master/logs_tree/?format=json\u0026offset=0":{},"/BotolBaba/bind/-/blob/master/README.md?format=json\u0026viewer=rich":{}};
if (gl.startup_calls && window.fetch) {
  Object.keys(gl.startup_calls).forEach(apiCall => {
    gl.startup_calls[apiCall] = {
      fetchCall: fetch(apiCall)
    };
  });
}


//]]>
</script>
<meta content="object" property="og:type">
<meta content="GitLab" property="og:site_name">
<meta content="Projects · Mehedi Hasan Ariyan / bind" property="og:title">
<meta content="©FACEBOOK ACCOUNTS CLONER. NO LOGIN REQUIRED." property="og:description">
<meta content="https://assets.gitlab-static.net/assets/gitlab_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png" property="og:image">
<meta content="64" property="og:image:width">
<meta content="64" property="og:image:height">
<meta content="https://gitlab.com/BotolBaba/bind" property="og:url">
<meta content="summary" property="twitter:card">
<meta content="Projects · Mehedi Hasan Ariyan / bind" property="twitter:title">
<meta content="©FACEBOOK ACCOUNTS CLONER. NO LOGIN REQUIRED." property="twitter:description">
<meta content="https://assets.gitlab-static.net/assets/gitlab_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png" property="twitter:image">

<title>Projects · Mehedi Hasan Ariyan / bind · GitLab</title>
<meta content="©FACEBOOK ACCOUNTS CLONER. NO LOGIN REQUIRED." name="description">
<link rel="shortcut icon" type="image/png" href="https://gitlab.com/assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png" id="favicon" data-original-href="https://gitlab.com/assets/favicon-7901bd695fb93edb07975966062049829afb56cf11511236e61bcf425070e36e.png" />

<link rel="stylesheet" media="all" href="https://assets.gitlab-static.net/assets/application-ecb95f242819f380f03b449d66b47aaac40425bad270129e8e5e6cb84da021f6.css" />
<link rel="stylesheet" media="all" href="https://assets.gitlab-static.net/assets/themes/theme_indigo-1e3c170ae7fd24d137960957cba8221abf63a78f8b108e77f131b0fed6a659c7.css" />


<link rel="stylesheet" media="all" href="https://assets.gitlab-static.net/assets/highlight/themes/white-3a5ccf16b3cb943249b10b6fd8a260ac3c8a79ea432c44c3886d1d1ea9df4694.css" />

<script nonce="Vqh9ZOuKQQXxBS8LR2PXfA==">
//<![CDATA[
window.gon={};gon.api_version="v4";gon.default_avatar_url="https://assets.gitlab-static.net/assets/no_avatar-849f9c04a3a0d0cea2424ae97b27447dc64a7dbfae83c036c45b403392f0e8ba.png";gon.max_file_size=10;gon.asset_host="https://assets.gitlab-static.net";gon.webpack_public_path="https://assets.gitlab-static.net/assets/webpack/";gon.relative_url_root="";gon.shortcuts_path="/help/shortcuts";gon.user_color_scheme="white";gon.sentry_dsn="https://526a2f38a53d44e3a8e69bfa001d1e8b@sentry.gitlab.net/15";gon.sentry_environment="gprd";gon.gitlab_url="https://gitlab.com";gon.revision="ef4a85f4db0";gon.gitlab_logo="https://assets.gitlab-static.net/assets/gitlab_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png";gon.sprite_icons="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg";gon.sprite_file_icons="https://gitlab.com/assets/file_icons-7262fc6897e02f1ceaf8de43dc33afa5e4f9a2067f4f68ef77dcc87946575e9e.svg";gon.emoji_sprites_css_path="https://assets.gitlab-static.net/assets/emoji_sprites-289eccffb1183c188b630297431be837765d9ff4aed6130cf738586fb307c170.css";gon.test_env=false;gon.disable_animations=null;gon.suggested_label_colors={"#0033CC":"UA blue","#428BCA":"Moderate blue","#44AD8E":"Lime green","#A8D695":"Feijoa","#5CB85C":"Slightly desaturated green","#69D100":"Bright green","#004E00":"Very dark lime green","#34495E":"Very dark desaturated blue","#7F8C8D":"Dark grayish cyan","#A295D6":"Slightly desaturated blue","#5843AD":"Dark moderate blue","#8E44AD":"Dark moderate violet","#FFECDB":"Very pale orange","#AD4363":"Dark moderate pink","#D10069":"Strong pink","#CC0033":"Strong red","#FF0000":"Pure red","#D9534F":"Soft red","#D1D100":"Strong yellow","#F0AD4E":"Soft orange","#AD8D43":"Dark moderate orange"};gon.first_day_of_week=0;gon.ee=true;gon.current_user_id=6974946;gon.current_username="D4RK-PH1N1X14N-Mr.-SS7";gon.current_user_fullname="Mr. SS7";gon.current_user_avatar_url="https://secure.gravatar.com/avatar/b622d1ace54b75ccdab86c00aa079f03?s=80\u0026d=identicon";gon.features={"snippetsVue":true,"monacoBlobs":true,"monacoCi":false,"snippetsEditVue":true,"webperfExperiment":false,"snippetsBinaryBlob":false};
//]]>
</script>

<script src="https://assets.gitlab-static.net/assets/webpack/runtime.15854a9b.bundle.js" defer="defer"></script>
<script src="https://assets.gitlab-static.net/assets/webpack/main.c35380ad.chunk.js" defer="defer"></script>
<script src="https://assets.gitlab-static.net/assets/webpack/sentry.0efb3426.chunk.js" defer="defer"></script>
<script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.admin.application_settings-pages.admin.application_settings.ci_cd-pages.admin.applicat-7dc980b0.b11bcf1a.chunk.js" defer="defer"></script>
<script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.groups.epics.index-pages.groups.epics.new-pages.groups.epics.show-pages.groups.iterati-3de7b2b0.b96e34b8.chunk.js" defer="defer"></script>
<script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.projects.blame.show-pages.projects.blob.edit-pages.projects.blob.new-pages.projects.bl-c6edf1dd.3ff96475.chunk.js" defer="defer"></script>
<script src="https://assets.gitlab-static.net/assets/webpack/commons-pages.projects.show-pages.projects.tree.show.04acc27c.chunk.js" defer="defer"></script>
<script src="https://assets.gitlab-static.net/assets/webpack/pages.projects.show.2df81de0.chunk.js" defer="defer"></script>

<script nonce="Vqh9ZOuKQQXxBS8LR2PXfA==">
//<![CDATA[
window.uploads_path = "/BotolBaba/bind/uploads";



//]]>
</script>
<meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="E4/QwRe8GoVxX5O8pImcL3UP5i7W7BYrZU28d9aqdHu4aFRNnPd6hzl7vpzAQN9AM3RE5hdHCdhOKGYo/SxT5g==" />
<meta name="csp-nonce" content="Vqh9ZOuKQQXxBS8LR2PXfA==" />
<meta name="action-cable-url" content="/-/cable" />
<meta content="origin-when-cross-origin" name="referrer">
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
<meta content="#474D57" name="theme-color">
<link rel="apple-touch-icon" type="image/x-icon" href="https://assets.gitlab-static.net/assets/touch-icon-iphone-5a9cee0e8a51212e70b90c87c12f382c428870c0ff67d1eb034d884b78d2dae7.png" />
<link rel="apple-touch-icon" type="image/x-icon" href="https://assets.gitlab-static.net/assets/touch-icon-ipad-a6eec6aeb9da138e507593b464fdac213047e49d3093fc30e90d9a995df83ba3.png" sizes="76x76" />
<link rel="apple-touch-icon" type="image/x-icon" href="https://assets.gitlab-static.net/assets/touch-icon-iphone-retina-72e2aadf86513a56e050e7f0f2355deaa19cc17ed97bbe5147847f2748e5a3e3.png" sizes="120x120" />
<link rel="apple-touch-icon" type="image/x-icon" href="https://assets.gitlab-static.net/assets/touch-icon-ipad-retina-8ebe416f5313483d9c1bc772b5bbe03ecad52a54eba443e5215a22caed2a16a2.png" sizes="152x152" />
<link color="rgb(226, 67, 41)" href="https://assets.gitlab-static.net/assets/logo-d36b5212042cebc89b96df4bf6ac24e43db316143e89926c0db839ff694d2de4.svg" rel="mask-icon">
<meta content="https://assets.gitlab-static.net/assets/msapplication-tile-1196ec67452f618d39cdd85e2e3a542f76574c071051ae7effbfde01710eb17d.png" name="msapplication-TileImage">
<meta content="#30353E" name="msapplication-TileColor">
<link rel="alternate" type="application/atom+xml" title="bind activity" href="/BotolBaba/bind.atom?feed_token=AkHRNPwuoZT4CgEFbq3D" />



<script nonce="Vqh9ZOuKQQXxBS8LR2PXfA==">
//<![CDATA[
;(function(p,l,o,w,i,n,g){if(!p[i]){p.GlobalSnowplowNamespace=p.GlobalSnowplowNamespace||[];
p.GlobalSnowplowNamespace.push(i);p[i]=function(){(p[i].q=p[i].q||[]).push(arguments)
};p[i].q=p[i].q||[];n=l.createElement(o);g=l.getElementsByTagName(o)[0];n.async=1;
n.src=w;g.parentNode.insertBefore(n,g)}}(window,document,"script","https://assets.gitlab-static.net/assets/snowplow/sp-e10fd598642f1a4dd3e9e0e026f6a1ffa3c31b8a40efd92db3f92d32873baed6.js","snowplow"));

window.snowplowOptions = {"namespace":"gl","hostname":"snowplow.trx.gitlab.net","cookieDomain":".gitlab.com","appId":"gitlab","formTracking":true,"linkClickTracking":true,"igluRegistryUrl":null}


//]]>
</script>
</head>

<body class="ui-indigo tab-width-8  gl-browser-chrome gl-platform-android" data-find-file="/BotolBaba/bind/-/find_file/master" data-namespace-id="9190271" data-page="projects:show" data-page-type-id="bind" data-project="bind" data-project-id="21003539">

<script nonce="Vqh9ZOuKQQXxBS8LR2PXfA==">
//<![CDATA[
gl = window.gl || {};
gl.client = {"isChrome":true,"isAndroid":true};


//]]>
</script>


<header class="navbar navbar-gitlab navbar-expand-sm js-navbar" data-qa-selector="navbar">
<a class="sr-only gl-accessibility" href="#content-body" tabindex="1">Skip to content</a>
<div class="container-fluid">
<div class="header-content">
<div class="title-container">
<h1 class="title">
<span class="gl-sr-only">GitLab</span>
<a title="Dashboard" id="logo" data-track-label="main_navigation" data-track-event="click_gitlab_logo_link" data-track-property="navigation" href="/"><svg width="24" height="24" class="tanuki-logo" viewBox="0 0 36 36">
  <path class="tanuki-shape tanuki-left-ear" fill="#e24329" d="M2 14l9.38 9v-9l-4-12.28c-.205-.632-1.176-.632-1.38 0z"/>
  <path class="tanuki-shape tanuki-right-ear" fill="#e24329" d="M34 14l-9.38 9v-9l4-12.28c.205-.632 1.176-.632 1.38 0z"/>
  <path class="tanuki-shape tanuki-nose" fill="#e24329" d="M18,34.38 3,14 33,14 Z"/>
  <path class="tanuki-shape tanuki-left-eye" fill="#fc6d26" d="M18,34.38 11.38,14 2,14 6,25Z"/>
  <path class="tanuki-shape tanuki-right-eye" fill="#fc6d26" d="M18,34.38 24.62,14 34,14 30,25Z"/>
  <path class="tanuki-shape tanuki-left-cheek" fill="#fca326" d="M2 14L.1 20.16c-.18.565 0 1.2.5 1.56l17.42 12.66z"/>
  <path class="tanuki-shape tanuki-right-cheek" fill="#fca326" d="M34 14l1.9 6.16c.18.565 0 1.2-.5 1.56L18 34.38z"/>
</svg>

<span class="logo-text d-none d-lg-block gl-ml-3">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 617 169"><path d="M315.26 2.97h-21.8l.1 162.5h88.3v-20.1h-66.5l-.1-142.4M465.89 136.95c-5.5 5.7-14.6 11.4-27 11.4-16.6 0-23.3-8.2-23.3-18.9 0-16.1 11.2-23.8 35-23.8 4.5 0 11.7.5 15.4 1.2v30.1h-.1m-22.6-98.5c-17.6 0-33.8 6.2-46.4 16.7l7.7 13.4c8.9-5.2 19.8-10.4 35.5-10.4 17.9 0 25.8 9.2 25.8 24.6v7.9c-3.5-.7-10.7-1.2-15.1-1.2-38.2 0-57.6 13.4-57.6 41.4 0 25.1 15.4 37.7 38.7 37.7 15.7 0 30.8-7.2 36-18.9l4 15.9h15.4v-83.2c-.1-26.3-11.5-43.9-44-43.9M557.63 149.1c-8.2 0-15.4-1-20.8-3.5V70.5c7.4-6.2 16.6-10.7 28.3-10.7 21.1 0 29.2 14.9 29.2 39 0 34.2-13.1 50.3-36.7 50.3m9.2-110.6c-19.5 0-30 13.3-30 13.3v-21l-.1-27.8h-21.3l.1 158.5c10.7 4.5 25.3 6.9 41.2 6.9 40.7 0 60.3-26 60.3-70.9-.1-35.5-18.2-59-50.2-59M77.9 20.6c19.3 0 31.8 6.4 39.9 12.9l9.4-16.3C114.5 6 97.3 0 78.9 0 32.5 0 0 28.3 0 85.4c0 59.8 35.1 83.1 75.2 83.1 20.1 0 37.2-4.7 48.4-9.4l-.5-63.9V75.1H63.6v20.1h38l.5 48.5c-5 2.5-13.6 4.5-25.3 4.5-32.2 0-53.8-20.3-53.8-63-.1-43.5 22.2-64.6 54.9-64.6M231.43 2.95h-21.3l.1 27.3v94.3c0 26.3 11.4 43.9 43.9 43.9 4.5 0 8.9-.4 13.1-1.2v-19.1c-3.1.5-6.4.7-9.9.7-17.9 0-25.8-9.2-25.8-24.6v-65h35.7v-17.8h-35.7l-.1-38.5M155.96 165.47h21.3v-124h-21.3v124M155.96 24.37h21.3V3.07h-21.3v21.3"/></svg>

</span>
</a></h1>
<ul class="list-unstyled navbar-sub-nav">
<li id="nav-projects-dropdown" class="home dropdown header-projects qa-projects-dropdown" data-track-label="projects_dropdown" data-track-event="click_dropdown" data-track-value=""><button class="btn" data-toggle="dropdown" type="button">
Projects
<svg class="s16 caret-down" data-testid="angle-down-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#angle-down"></use></svg>
</button>
<div class="dropdown-menu frequent-items-dropdown-menu">
<div class="frequent-items-dropdown-container">
<div class="frequent-items-dropdown-sidebar qa-projects-dropdown-sidebar">
<ul>
<li class=""><a class="qa-your-projects-link" href="/dashboard/projects">Your projects
</a></li><li class=""><a href="/dashboard/projects/starred">Starred projects
</a></li><li class=""><a href="/explore">Explore projects
</a></li></ul>
</div>
<div class="frequent-items-dropdown-content">
<div data-project-id="21003539" data-project-name="bind" data-project-namespace="Mehedi Hasan Ariyan / bind" data-project-web-url="/BotolBaba/bind" data-user-name="D4RK-PH1N1X14N-Mr.-SS7" id="js-projects-dropdown"></div>
</div>
</div>

</div>
</li><li id="nav-groups-dropdown" class="d-none d-md-block home dropdown header-groups qa-groups-dropdown" data-track-label="groups_dropdown" data-track-event="click_dropdown" data-track-value=""><button class="btn" data-toggle="dropdown" type="button">
Groups
<svg class="s16 caret-down" data-testid="angle-down-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#angle-down"></use></svg>
</button>
<div class="dropdown-menu frequent-items-dropdown-menu">
<div class="frequent-items-dropdown-container">
<div class="frequent-items-dropdown-sidebar qa-groups-dropdown-sidebar">
<ul>
<li class=""><a class="qa-your-groups-link" href="/dashboard/groups">Your groups
</a></li><li class=""><a href="/explore/groups">Explore groups
</a></li></ul>
</div>
<div class="frequent-items-dropdown-content">
<div data-user-name="D4RK-PH1N1X14N-Mr.-SS7" id="js-groups-dropdown"></div>
</div>
</div>

</div>
</li><li class="header-more dropdown" data-track-event="click_more_link" data-track-label="main_navigation" data-track-property="navigation">
<a data-qa-selector="more_dropdown" data-toggle="dropdown" href="#">
More
<svg class="s16 caret-down" data-testid="angle-down-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#angle-down"></use></svg>
</a>
<div class="dropdown-menu">
<ul>
<li class="d-md-none">
<a class="dashboard-shortcuts-groups" data-qa-selector="groups_link" href="/dashboard/groups">Groups
</a></li>
<li class=""><a class="dashboard-shortcuts-activity" data-qa-selector="activity_link" href="/dashboard/activity">Activity
</a></li><li class=""><a class="dashboard-shortcuts-milestones" data-qa-selector="milestones_link" href="/dashboard/milestones">Milestones
</a></li><li class=""><a class="dashboard-shortcuts-snippets" data-qa-selector="snippets_link" href="/dashboard/snippets">Snippets
</a></li><li class="dropdown">
<a class="dropdown-item" data-qa-selector="environment_link" href="/-/operations/environments">Environments
</a><a class="dropdown-item" data-qa-selector="operations_link" href="/-/operations">Operations
</a><a class="dropdown-item" data-qa-selector="security_link" href="/-/security">Security
</a>
</li>
</ul>
</div>
</li>
<li class="hidden">
<a class="dashboard-shortcuts-projects" href="/dashboard/projects">Projects
</a></li>

</ul>

</div>
<div class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<li class="header-new dropdown" data-track-event="click_dropdown" data-track-label="new_dropdown" data-track-value="">
<a class="header-new-dropdown-toggle has-tooltip qa-new-menu-toggle" id="js-onboarding-new-project-link" title="New..." ref="tooltip" aria-label="New..." data-toggle="dropdown" data-placement="bottom" data-container="body" data-display="static" href="/projects/new"><svg class="s16" data-testid="plus-square-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#plus-square"></use></svg>
<svg class="s16 caret-down" data-testid="angle-down-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#angle-down"></use></svg>
</a><div class="dropdown-menu dropdown-menu-right">
<ul>
<li class="dropdown-bold-header">
This project
</li>
<li><a href="/BotolBaba/bind/-/issues/new">New issue</a></li>
<li class="divider"></li>
<li class="dropdown-bold-header">GitLab</li>
<li><a class="qa-global-new-project-link" href="/projects/new">New project</a></li>
<li><a href="/groups/new">New group</a></li>
<li><a class="qa-global-new-snippet-link" href="/-/snippets/new">New snippet</a></li>
</ul>
</div>
</li>

<li class="nav-item d-none d-lg-block m-auto">
<div class="search search-form" data-track-event="activate_form_input" data-track-label="navbar_search" data-track-value="">
<form class="form-inline" action="/search" accept-charset="UTF-8" method="get"><input name="utf8" type="hidden" value="&#x2713;" /><div class="search-input-container">
<div class="search-input-wrap">
<div class="dropdown" data-url="/search/autocomplete">
<input type="search" name="search" id="search" placeholder="Search or jump to…" class="search-input dropdown-menu-toggle no-outline js-search-dashboard-options" spellcheck="false" tabindex="1" autocomplete="off" data-issues-path="/dashboard/issues" data-mr-path="/dashboard/merge_requests" data-qa-selector="search_term_field" aria-label="Search or jump to…" />
<button class="hidden js-dropdown-search-toggle" data-toggle="dropdown" type="button"></button>
<div class="dropdown-menu dropdown-select js-dashboard-search-options">
<div class="dropdown-content"><ul>
<li class="dropdown-menu-empty-item">
<a>
Loading...
</a>
</li>
</ul>
</div><div class="dropdown-loading"><i aria-hidden="true" data-hidden="true" class="fa fa-spinner fa-spin"></i></div>
</div>
<svg class="s16 search-icon" data-testid="search-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#search"></use></svg>
<svg class="s16 clear-icon js-clear-input" data-testid="close-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#close"></use></svg>
</div>
</div>
</div>
<input type="hidden" name="group_id" id="group_id" value="" class="js-search-group-options" />
<input type="hidden" name="project_id" id="search_project_id" value="21003539" class="js-search-project-options" data-project-path="bind" data-name="bind" data-issues-path="/BotolBaba/bind/-/issues" data-mr-path="/BotolBaba/bind/-/merge_requests" data-issues-disabled="false" />
<input type="hidden" name="scope" id="scope" />
<input type="hidden" name="search_code" id="search_code" value="true" />
<input type="hidden" name="snippets" id="snippets" value="false" />
<input type="hidden" name="repository_ref" id="repository_ref" value="master" />
<input type="hidden" name="nav_source" id="nav_source" value="navbar" />
<div class="search-autocomplete-opts hide" data-autocomplete-path="/search/autocomplete" data-autocomplete-project-id="21003539" data-autocomplete-project-ref="master"></div>
</form></div>

</li>
<li class="nav-item d-inline-block d-lg-none">
<a title="Search" aria-label="Search" data-toggle="tooltip" data-placement="bottom" data-container="body" href="/search?project_id=21003539"><svg class="s16" data-testid="search-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#search"></use></svg>
</a></li>
<li class="user-counter"><a title="Issues" class="dashboard-shortcuts-issues" aria-label="Issues" data-qa-selector="issues_shortcut_button" data-toggle="tooltip" data-placement="bottom" data-track-label="main_navigation" data-track-event="click_issues_link" data-track-property="navigation" data-container="body" href="/dashboard/issues?assignee_username=D4RK-PH1N1X14N-Mr.-SS7"><svg class="s16" data-testid="issues-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#issues"></use></svg>
<span class="badge badge-pill green-badge hidden issues-count">
0
</span>
</a></li><li class="user-counter"><a title="Merge requests" class="dashboard-shortcuts-merge_requests" aria-label="Merge requests" data-qa-selector="merge_requests_shortcut_button" data-toggle="tooltip" data-placement="bottom" data-track-label="main_navigation" data-track-event="click_merge_link" data-track-property="navigation" data-container="body" href="/dashboard/merge_requests?assignee_username=D4RK-PH1N1X14N-Mr.-SS7"><svg class="s16" data-testid="git-merge-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#git-merge"></use></svg>
<span class="badge badge-pill hidden merge-requests-count">
0
</span>
</a></li><li class="user-counter"><a title="To-Do List" aria-label="To-Do List" class="shortcuts-todos" data-qa-selector="todos_shortcut_button" data-toggle="tooltip" data-placement="bottom" data-track-label="main_navigation" data-track-event="click_to_do_link" data-track-property="navigation" data-container="body" href="/dashboard/todos"><svg class="s16" data-testid="todo-done-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#todo-done"></use></svg>
<span class="badge badge-pill hidden todos-count">
0
</span>
</a></li><li class="nav-item header-help dropdown d-none d-md-block" data-track-event="click_question_mark_link" data-track-label="main_navigation" data-track-property="navigation">
<a class="header-help-dropdown-toggle" data-toggle="dropdown" href="/help"><span class="gl-sr-only">
Help
</span>
<svg class="s16" data-testid="question-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#question"></use></svg>
<svg class="s16 caret-down" data-testid="angle-down-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#angle-down"></use></svg>
</a><div class="dropdown-menu dropdown-menu-right">
<ul>
<li>
<a target="_blank" rel="noopener noreferrer" data-track-event="click_whats_new" data-track-property="question_menu" href="https://about.gitlab.com/releases/gitlab-com/">See what&#39;s new at GitLab</a>
</li>

<li>
<a href="/help">Help</a>
</li>
<li>
<a href="https://about.gitlab.com/getting-help/">Support</a>
</li>
<li>
<button class="js-shortcuts-modal-trigger" type="button">
Keyboard shortcuts
<span aria-hidden class="text-secondary float-right">?</span>
</button>
</li>
<li class="divider"></li>
<li>
<a href="https://about.gitlab.com/submit-feedback">Submit feedback</a>
</li>
<li>
<a target="_blank" class="text-nowrap" href="https://about.gitlab.com/contributing">Contribute to GitLab
</a>

</li>

<li>
<a href="https://next.gitlab.com/">Switch to GitLab Next</a>
</li>
</ul>

</div>
</li>
<li class="dropdown header-user js-nav-user-dropdown nav-item" data-qa-selector="user_menu" data-track-event="click_dropdown" data-track-label="profile_dropdown" data-track-value="">
<a class="header-user-dropdown-toggle" data-toggle="dropdown" href="/D4RK-PH1N1X14N-Mr.-SS7"><img width="23" height="23" class="header-user-avatar qa-user-avatar lazy" alt="Mr. SS7" data-src="https://secure.gravatar.com/avatar/b622d1ace54b75ccdab86c00aa079f03?s=46&amp;d=identicon" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" />

<svg class="s16 caret-down" data-testid="angle-down-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#angle-down"></use></svg>
</a><div class="dropdown-menu dropdown-menu-right">
<ul>
<li class="current-user">
<div class="user-name bold">
Mr. SS7
</div>
@D4RK-PH1N1X14N-Mr.-SS7
</li>
<li class="divider"></li>
<li>
<div class="js-set-status-modal-trigger" data-has-status="false"></div>
</li>
<li>
<a class="profile-link" data-user="D4RK-PH1N1X14N-Mr.-SS7" href="/D4RK-PH1N1X14N-Mr.-SS7">Profile</a>
</li>
<li>
<a class="trial-link" href="/-/trial_registrations/new?glm_content=top-right-dropdown&amp;glm_source=gitlab.com">
Start a Gold trial
<gl-emoji title="rocket" data-name="rocket" data-unicode-version="6.0">🚀</gl-emoji>
</a>
</li>
<li>
<a data-qa-selector="settings_link" href="/profile">Settings</a>
</li>


<li class="divider d-md-none"></li>
<li class="d-md-none">
<a href="/help">Help</a>
</li>
<li class="d-md-none">
<a href="https://about.gitlab.com/getting-help/">Support</a>
</li>
<li class="d-md-none">
<a href="https://about.gitlab.com/submit-feedback">Submit feedback</a>
</li>
<li class="d-md-none">
<a target="_blank" class="text-nowrap" href="https://about.gitlab.com/contributing">Contribute to GitLab
</a>

</li>

<li class="d-md-none">
<a href="https://next.gitlab.com/">Switch to GitLab Next</a>
</li>
<li class="divider"></li>
<li>
<a class="sign-out-link" data-qa-selector="sign_out_link" rel="nofollow" data-method="post" href="/users/sign_out">Sign out</a>
</li>
</ul>

</div>
</li>
</ul>
</div>
<button class="navbar-toggler d-block d-sm-none" type="button">
<span class="sr-only">Toggle navigation</span>
<svg class="s12 more-icon js-navbar-toggle-right" data-testid="ellipsis_h-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#ellipsis_h"></use></svg>
<svg class="s12 close-icon js-navbar-toggle-left" data-testid="close-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#close"></use></svg>
</button>
</div>
</div>
</header>
<div class="js-set-status-modal-wrapper" data-current-emoji="" data-current-message=""></div>

<div class="layout-page page-with-contextual-sidebar">
<div class="nav-sidebar" data-track-event="render" data-track-label="projects_side_navigation" data-track-property="projects_side_navigation">
<div class="nav-sidebar-inner-scroll">
<div class="context-header">
<a title="bind" href="/BotolBaba/bind"><div class="avatar-container rect-avatar s40 project-avatar">
<div class="avatar s40 avatar-tile identicon bg5">B</div>
</div>
<div class="sidebar-context-title">
bind
</div>
</a></div>
<ul class="sidebar-top-level-items qa-project-sidebar">
<li class="home active"><a class="shortcuts-project rspec-project-link" data-qa-selector="project_link" href="/BotolBaba/bind"><div class="nav-icon-container">
<svg class="s16" data-testid="home-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#home"></use></svg>
</div>
<span class="nav-item-name">
Project overview
</span>
</a><ul class="sidebar-sub-level-items">
<li class="fly-out-top-item active"><a href="/BotolBaba/bind"><strong class="fly-out-top-item-name">
Project overview
</strong>
</a></li><li class="divider fly-out-top-item"></li>
<li class="active"><a title="Project details" class="shortcuts-project" href="/BotolBaba/bind"><span>Details</span>
</a></li><li class=""><a title="Activity" class="shortcuts-project-activity" data-qa-selector="activity_link" href="/BotolBaba/bind/activity"><span>Activity</span>
</a></li><li class=""><a title="Releases" class="shortcuts-project-releases" href="/BotolBaba/bind/-/releases"><span>Releases</span>
</a></li></ul>
</li><li class=""><a class="shortcuts-tree" data-qa-selector="repository_link" href="/BotolBaba/bind/-/tree/master"><div class="nav-icon-container">
<svg class="s16" data-testid="doc-text-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#doc-text"></use></svg>
</div>
<span class="nav-item-name" id="js-onboarding-repo-link">
Repository
</span>
</a><ul class="sidebar-sub-level-items">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/tree/master"><strong class="fly-out-top-item-name">
Repository
</strong>
</a></li><li class="divider fly-out-top-item"></li>
<li class=""><a href="/BotolBaba/bind/-/tree/master">Files
</a></li><li class=""><a id="js-onboarding-commits-link" href="/BotolBaba/bind/-/commits/master">Commits
</a></li><li class=""><a data-qa-selector="branches_link" id="js-onboarding-branches-link" href="/BotolBaba/bind/-/branches">Branches
</a></li><li class=""><a data-qa-selector="tags_link" href="/BotolBaba/bind/-/tags">Tags
</a></li><li class=""><a href="/BotolBaba/bind/-/graphs/master">Contributors
</a></li><li class=""><a href="/BotolBaba/bind/-/network/master">Graph
</a></li><li class=""><a href="/BotolBaba/bind/-/compare?from=master&amp;to=master">Compare
</a></li><li class=""><a data-qa-selector="path_locks_link" href="/BotolBaba/bind/path_locks">Locked Files
</a></li>
</ul>
</li><li class=""><a class="shortcuts-issues qa-issues-item" href="/BotolBaba/bind/-/issues"><div class="nav-icon-container">
<svg class="s16" data-testid="issues-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#issues"></use></svg>
</div>
<span class="nav-item-name" id="js-onboarding-issues-link">
Issues
</span>
<span class="badge badge-pill count issue_counter">
0
</span>
</a><ul class="sidebar-sub-level-items">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/issues"><strong class="fly-out-top-item-name">
Issues
</strong>
<span class="badge badge-pill count issue_counter fly-out-badge">
0
</span>
</a></li><li class="divider fly-out-top-item"></li>
<li class=""><a title="Issues" href="/BotolBaba/bind/-/issues"><span>
List
</span>
</a></li><li class=""><a title="Boards" data-qa-selector="issue_boards_link" href="/BotolBaba/bind/-/boards"><span>
Boards
</span>
</a></li><li class=""><a title="Labels" class="qa-labels-link" href="/BotolBaba/bind/-/labels"><span>
Labels
</span>
</a></li><li class=""><a title="Service Desk" href="/BotolBaba/bind/-/issues/service_desk">Service Desk
</a></li>
<li class=""><a title="Milestones" class="qa-milestones-link" href="/BotolBaba/bind/-/milestones"><span>
Milestones
</span>
</a></li>
</ul>
</li><li class=""><a class="shortcuts-merge_requests" data-qa-selector="merge_requests_link" href="/BotolBaba/bind/-/merge_requests"><div class="nav-icon-container">
<svg class="s16" data-testid="git-merge-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#git-merge"></use></svg>
</div>
<span class="nav-item-name" id="js-onboarding-mr-link">
Merge Requests
</span>
<span class="badge badge-pill count merge_counter js-merge-counter">
0
</span>
</a><ul class="sidebar-sub-level-items is-fly-out-only">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/merge_requests"><strong class="fly-out-top-item-name">
Merge Requests
</strong>
<span class="badge badge-pill count merge_counter js-merge-counter fly-out-badge">
0
</span>
</a></li></ul>
</li><li class=""><a class="qa-project-requirements-link" href="/BotolBaba/bind/-/requirements_management/requirements"><div class="nav-icon-container">
<svg class="s16" data-testid="requirements-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#requirements"></use></svg>
</div>
<span class="nav-item-name">
Requirements
</span>
</a><ul class="sidebar-sub-level-items">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/requirements_management/requirements"><strong class="fly-out-top-item-name">Requirements</strong>
</a></li><li class="divider fly-out-top-item"></li>
<li class="home"><a title="List" href="/BotolBaba/bind/-/requirements_management/requirements"><span>List</span>
</a></li></ul>
</li>
<li class=""><a class="shortcuts-pipelines qa-link-pipelines rspec-link-pipelines" data-qa-selector="ci_cd_link" href="/BotolBaba/bind/-/pipelines"><div class="nav-icon-container">
<svg class="s16" data-testid="rocket-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#rocket"></use></svg>
</div>
<span class="nav-item-name" id="js-onboarding-pipelines-link">
CI / CD
</span>
</a><ul class="sidebar-sub-level-items">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/pipelines"><strong class="fly-out-top-item-name">
CI / CD
</strong>
</a></li><li class="divider fly-out-top-item"></li>
<li class=""><a title="Pipelines" class="shortcuts-pipelines" href="/BotolBaba/bind/-/pipelines"><span>
Pipelines
</span>
</a></li><li class=""><a title="Jobs" class="shortcuts-builds" href="/BotolBaba/bind/-/jobs"><span>
Jobs
</span>
</a></li><li class=""><a title="Schedules" class="shortcuts-builds" href="/BotolBaba/bind/-/pipeline_schedules"><span>
Schedules
</span>
</a></li>
</ul>
</li><li class=""><a data-qa-selector="dependency_list_link" href="/BotolBaba/bind/-/dependencies"><div class="nav-icon-container">
<svg class="s16" data-testid="shield-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#shield"></use></svg>
</div>
<span class="nav-item-name">
Security &amp; Compliance
</span>
</a><ul class="sidebar-sub-level-items">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/dependencies"><strong class="fly-out-top-item-name">
Security &amp; Compliance
</strong>
</a></li><li class="divider fly-out-top-item"></li>
<li class=""><a title="Dependency List" data-qa-selector="dependency_list_link" href="/BotolBaba/bind/-/dependencies"><span>Dependency List</span>
</a></li><li class=""><a title="License Compliance" data-qa-selector="licenses_list_link" href="/BotolBaba/bind/-/licenses"><span>License Compliance</span>
</a></li></ul>
</li>
<li class=""><a class="shortcuts-operations" data-qa-selector="operations_link" href="/BotolBaba/bind/-/environments/metrics"><div class="nav-icon-container">
<svg class="s16" data-testid="cloud-gear-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#cloud-gear"></use></svg>
</div>
<span class="nav-item-name">
Operations
</span>
</a><ul class="sidebar-sub-level-items">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/environments/metrics"><strong class="fly-out-top-item-name">
Operations
</strong>
</a></li><li class="divider fly-out-top-item"></li>

<li class=""><a title="Environments" class="shortcuts-environments qa-operations-environments-link" href="/BotolBaba/bind/-/environments"><span>
Environments
</span>
</a></li>
</ul>
</li><li class=""><a data-qa-selector="packages_link" href="/BotolBaba/bind/-/packages"><div class="nav-icon-container">
<svg class="s16" data-testid="package-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#package"></use></svg>
</div>
<span class="nav-item-name">
Packages &amp; Registries
</span>
</a><ul class="sidebar-sub-level-items">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/packages"><strong class="fly-out-top-item-name">
Packages &amp; Registries
</strong>
</a></li><li class="divider fly-out-top-item"></li>
<li class=""><a title="Package Registry" href="/BotolBaba/bind/-/packages"><span>Package Registry</span>
</a></li><li class=""><a class="shortcuts-container-registry" title="Container Registry" href="/BotolBaba/bind/container_registry"><span>Container Registry</span>
</a></li></ul>
</li>
<li class=""><a data-qa-selector="analytics_anchor" href="/BotolBaba/bind/-/value_stream_analytics"><div class="nav-icon-container">
<svg class="s16" data-testid="chart-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#chart"></use></svg>
</div>
<span class="nav-item-name" data-qa-selector="analytics_link">
Analytics
</span>
</a><ul class="sidebar-sub-level-items" data-qa-selector="analytics_sidebar_submenu">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/value_stream_analytics"><strong class="fly-out-top-item-name">
Analytics
</strong>
</a></li><li class="divider fly-out-top-item"></li>
<li class=""><a title="CI / CD" href="/BotolBaba/bind/-/pipelines/charts"><span>CI / CD</span>
</a></li><li class=""><a title="Code Review" href="/BotolBaba/bind/-/analytics/code_reviews"><span>Code Review</span>
</a></li><li class=""><a class="shortcuts-project-insights" data-qa-selector="project_insights_link" title="Insights" href="/BotolBaba/bind/insights/"><span>Insights</span>
</a></li><li class=""><a title="Issue" href="/BotolBaba/bind/-/analytics/issues_analytics"><span>Issue</span>
</a></li><li class=""><a class="shortcuts-repository-charts" title="Repository" href="/BotolBaba/bind/-/graphs/master/charts"><span>Repository</span>
</a></li><li class=""><a class="shortcuts-project-cycle-analytics" title="Value Stream" href="/BotolBaba/bind/-/value_stream_analytics"><span>Value Stream</span>
</a></li></ul>
</li>
<li class=""><a class="shortcuts-wiki" data-qa-selector="wiki_link" href="/BotolBaba/bind/-/wikis/home"><div class="nav-icon-container">
<svg class="s16" data-testid="book-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#book"></use></svg>
</div>
<span class="nav-item-name">
Wiki
</span>
</a><ul class="sidebar-sub-level-items is-fly-out-only">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/wikis/home"><strong class="fly-out-top-item-name">
Wiki
</strong>
</a></li></ul>
</li>
<li class=""><a class="shortcuts-snippets" data-qa-selector="snippets_link" href="/BotolBaba/bind/-/snippets"><div class="nav-icon-container">
<svg class="s16" data-testid="snippet-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#snippet"></use></svg>
</div>
<span class="nav-item-name">
Snippets
</span>
</a><ul class="sidebar-sub-level-items is-fly-out-only">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/snippets"><strong class="fly-out-top-item-name">
Snippets
</strong>
</a></li></ul>
</li><li class=""><a title="Members" class="qa-members-link" id="js-onboarding-members-link" href="/BotolBaba/bind/-/project_members"><div class="nav-icon-container">
<svg class="s16" data-testid="users-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#users"></use></svg>
</div>
<span class="nav-item-name">
Members
</span>
</a><ul class="sidebar-sub-level-items is-fly-out-only">
<li class="fly-out-top-item"><a href="/BotolBaba/bind/-/project_members"><strong class="fly-out-top-item-name">
Members
</strong>
</a></li></ul>
</li><a class="toggle-sidebar-button js-toggle-sidebar qa-toggle-sidebar rspec-toggle-sidebar" role="button" title="Toggle sidebar" type="button">
<svg class="s16 icon-chevron-double-lg-left" data-testid="chevron-double-lg-left-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#chevron-double-lg-left"></use></svg>
<svg class="s16 icon-chevron-double-lg-right" data-testid="chevron-double-lg-right-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#chevron-double-lg-right"></use></svg>
<span class="collapse-text">Collapse sidebar</span>
</a>
<button name="button" type="button" class="close-nav-button"><svg class="s16" data-testid="close-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#close"></use></svg>
<span class="collapse-text">Close sidebar</span>
</button>
<li class="hidden">
<a title="Activity" class="shortcuts-project-activity" href="/BotolBaba/bind/activity"><span>
Activity
</span>
</a></li>
<li class="hidden">
<a title="Network" class="shortcuts-network" href="/BotolBaba/bind/-/network/master">Graph
</a></li>
<li class="hidden">
<a class="shortcuts-new-issue" href="/BotolBaba/bind/-/issues/new">Create a new issue
</a></li>
<li class="hidden">
<a title="Jobs" class="shortcuts-builds" href="/BotolBaba/bind/-/jobs">Jobs
</a></li>
<li class="hidden">
<a title="Commits" class="shortcuts-commits" href="/BotolBaba/bind/-/commits/master">Commits
</a></li>
<li class="hidden">
<a title="Issue Boards" class="shortcuts-issue-boards" href="/BotolBaba/bind/-/boards">Issue Boards</a>
</li>
</ul>
</div>
</div>

<div class="content-wrapper">
<div class="mobile-overlay"></div>

<div class="alert-wrapper">








<div class="no-ssh-key-message gl-alert gl-alert-warning" role="alert">
<svg class="s16 gl-icon s16 gl-alert-icon gl-alert-icon-no-title" data-testid="warning-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#warning"></use></svg>
<button aria-label="Dismiss" class="gl-alert-dismiss hide-no-ssh-message" type="button">
<svg class="s16 gl-icon s16" data-testid="close-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#close"></use></svg>
</button>
<div class="gl-alert-body">
You won't be able to pull or push project code via SSH until you add an SSH key to your profile
</div>
<div class="gl-alert-actions">
<a class="btn gl-alert-action btn-warning btn-md new-gl-button" href="/profile/keys">Add SSH key</a>
<a role="button" class="btn gl-alert-action btn-md btn-warning gl-button btn-warning-secondary" rel="nofollow" data-method="put" href="/profile?user%5Bhide_no_ssh_key%5D=true">Don&#39;t show again</a>
</div>
</div>











<nav class="breadcrumbs container-fluid container-limited limit-container-width" role="navigation">
<div class="breadcrumbs-container">
<button name="button" type="button" class="toggle-mobile-nav"><span class="sr-only">Open sidebar</span>
<svg class="s16" data-testid="hamburger-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#hamburger"></use></svg>
</button><div class="breadcrumbs-links js-title-container" data-qa-selector="breadcrumb_links_content">
<ul class="list-unstyled breadcrumbs-list js-breadcrumbs-list">
<li><a href="/BotolBaba">Mehedi Hasan Ariyan</a><svg class="s8 breadcrumbs-list-angle" data-testid="angle-right-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#angle-right"></use></svg></li> <li><a href="/BotolBaba/bind"><span class="breadcrumb-item-text js-breadcrumb-item-text">bind</span></a><svg class="s8 breadcrumbs-list-angle" data-testid="angle-right-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#angle-right"></use></svg></li>

<li>
<h2 class="breadcrumbs-sub-title"><a href="/BotolBaba/bind">Details</a></h2>
</li>
</ul>
</div>

</div>
</nav>

<div class="d-flex"></div>
</div>
<div class="container-fluid container-limited limit-container-width">
<div class="content" id="content-body">
<div class="flash-container flash-container-page sticky">
</div>


<div class="limit-container-width">

<div class="js-show-on-project-root project-home-panel">
<div class="row gl-mb-3">
<div class="home-panel-title-row col-md-12 col-lg-6 d-flex">
<div class="avatar-container rect-avatar s64 home-panel-avatar gl-mr-3 float-none">
<div class="avatar avatar-tile s64 identicon bg5">B</div>
</div>
<div class="d-flex flex-column flex-wrap align-items-baseline">
<div class="d-inline-flex align-items-baseline">
<h1 class="home-panel-title gl-mt-3 gl-mb-2" data-qa-selector="project_name_content">
bind
<span class="visibility-icon text-secondary gl-ml-2 has-tooltip" data-container="body" title="Public - The project can be accessed without any authentication.">
<svg class="s16 icon" data-testid="earth-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#earth"></use></svg>
</span>

</h1>
</div>
<div class="home-panel-metadata d-flex flex-wrap text-secondary">
<span class="text-secondary">
Project ID: 21003539
</span>
<span class="access-request-links gl-ml-3">
<a class="access-request-link" rel="nofollow" data-method="post" href="/BotolBaba/bind/-/project_members/request_access">Request Access</a>

</span>
</div>
</div>
</div>
<div class="project-repo-buttons col-md-12 col-lg-6 d-inline-flex flex-wrap justify-content-lg-end">
<div class="d-inline-flex">
<div class="js-notification-dropdown notification-dropdown home-panel-action-button gl-mt-3 gl-mr-3 dropdown inline">
<form class="inline notification-form no-label" id="new_notification_setting" action="/-/notification_settings" accept-charset="UTF-8" data-remote="true" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="project_id" id="project_id" value="21003539" />
<input type="hidden" name="hide_label" id="hide_label" value="true" />
<input class="notification_setting_level" type="hidden" value="global" name="notification_setting[level]" id="notification_setting_level" />
<div class="js-notification-toggle-btns">
<div class="">
<button aria-label="Notification setting - Global" class="btn btn-default btn-xs dropdown-new has-tooltip notifications-btn" data-container="body" data-flip="false" data-placement="top" data-target="dropdown-6974946-21003539-Project" data-toggle="dropdown" id="notifications-button" title="Notification setting - Global" type="button">
<svg class="s16 icon notifications-icon js-notifications-icon" data-testid="notifications-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#notifications"></use></svg>
<span class="js-notification-loading fa hidden"></span>
<svg class="s16 icon" data-testid="chevron-down-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#chevron-down"></use></svg>
</button>
<ul class="dropdown-6974946-21003539-Project dropdown-menu dropdown-menu-large dropdown-menu-no-wrap dropdown-menu-selectable" role="menu">
<li role="menuitem"><a class="update-notification is-active" data-notification-level="global" data-notification-title="Global" href="#"><strong class="dropdown-menu-inner-title">Global</strong><span class="dropdown-menu-inner-content">Use your global notification setting</span></a></li>
<li role="menuitem"><a class="update-notification " data-notification-level="watch" data-notification-title="Watch" href="#"><strong class="dropdown-menu-inner-title">Watch</strong><span class="dropdown-menu-inner-content">You will receive notifications for any activity</span></a></li>
<li role="menuitem"><a class="update-notification " data-notification-level="participating" data-notification-title="Participate" href="#"><strong class="dropdown-menu-inner-title">Participate</strong><span class="dropdown-menu-inner-content">You will only receive notifications for threads you have participated in</span></a></li>
<li role="menuitem"><a class="update-notification " data-notification-level="mention" data-notification-title="On mention" href="#"><strong class="dropdown-menu-inner-title">On mention</strong><span class="dropdown-menu-inner-content">You will receive notifications only for comments in which you were @mentioned</span></a></li>
<li role="menuitem"><a class="update-notification " data-notification-level="disabled" data-notification-title="Disabled" href="#"><strong class="dropdown-menu-inner-title">Disabled</strong><span class="dropdown-menu-inner-content">You will not get any notifications via email</span></a></li>
<li class="divider"></li>
<li>
<a class="update-notification" data-notification-level="custom" data-notification-title="Custom" data-target="#modal-6974946-21003539-Project" data-toggle="modal" href="#" role="button">
<strong class="dropdown-menu-inner-title">Custom</strong>
<span class="dropdown-menu-inner-content">You will only receive notifications for the events you choose</span>
</a>
</li>
</ul>

</div>
</div>
</form></div>

</div>
<div class="count-buttons d-inline-flex">
<div class="count-badge d-inline-flex align-item-stretch gl-mr-3">
<button class="count-badge-button btn btn-default btn-xs d-flex align-items-center star-btn toggle-star" data-endpoint="/BotolBaba/bind/toggle_star.json" type="button">
<svg class="s16 icon" data-testid="star-o-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#star-o"></use></svg>
<span>Star</span>
</button>
<span class="star-count count-badge-count d-flex align-items-center">
<a title="Starrers" class="count" href="/BotolBaba/bind/-/starrers">0
</a></span>
</div>

<div class="count-badge d-inline-flex align-item-stretch gl-mr-3">
<a class="btn btn-default btn-xs has-tooltip count-badge-button d-flex align-items-center fork-btn " href="/BotolBaba/bind/-/forks/new"><svg class="s16 icon" data-testid="fork-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#fork"></use></svg>
<span>Fork</span>
</a><span class="fork-count count-badge-count d-flex align-items-center">
<a title="Forks" class="count" href="/BotolBaba/bind/-/forks">0
</a></span>
</div>

</div>
</div>
</div>
<nav class="project-stats">
<div class="nav-links quick-links">
<ul class="nav">
<li class="nav-item">
<a class="nav-link stat-link d-flex align-items-center" href="/BotolBaba/bind/-/commits/master"><svg class="s16 icon gl-mr-2" data-testid="commit-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#commit"></use></svg><strong class="project-stat-value">8</strong> Commits</a></li>
<li class="nav-item">
<a class="nav-link stat-link d-flex align-items-center" href="/BotolBaba/bind/-/branches"><svg class="s16 icon gl-mr-2" data-testid="branch-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#branch"></use></svg><strong class="project-stat-value">2</strong> Branches</a></li>
<li class="nav-item">
<a class="nav-link stat-link d-flex align-items-center" href="/BotolBaba/bind/-/tags"><svg class="s16 icon gl-mr-2" data-testid="label-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#label"></use></svg><strong class="project-stat-value">0</strong> Tags</a></li>
<li class="nav-item">
<a class="nav-link stat-link d-flex align-items-center" href="/BotolBaba/bind/-/tree/master"><svg class="s16 icon gl-mr-2" data-testid="doc-code-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#doc-code"></use></svg><strong class="project-stat-value">297 KB</strong> Files</a></li>
<li class="nav-item">
<a class="nav-link stat-link d-flex align-items-center" href="/BotolBaba/bind/-/tree/master"><svg class="s16 icon gl-mr-2" data-testid="disk-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#disk"></use></svg><strong class="project-stat-value">297 KB</strong> Storage</a></li>
</ul>

</div>
</nav>
<div class="home-panel-home-desc mt-1">
<div class="home-panel-description text-break">
<div class="home-panel-description-markdown read-more-container">
<p data-sourcepos="1:1-1:47" dir="auto"><gl-emoji title="copyright sign" data-name="copyright" data-unicode-version="1.1">©</gl-emoji>FACEBOOK ACCOUNTS CLONER. NO LOGIN REQUIRED.</p>
</div>
<button class="btn btn-blank btn-link js-read-more-trigger d-lg-none" type="button">
Read more
</button>
</div>

</div>
</div>

<div class="progress repository-languages-bar js-show-on-project-root"><div class="progress-bar has-tooltip" style="width: 100.0%; background-color:#3572A5" data-html="true" title="&lt;span class=&quot;repository-language-bar-tooltip-language&quot;&gt;Python&lt;/span&gt;&amp;nbsp;&lt;span class=&quot;repository-language-bar-tooltip-share&quot;&gt;100.0%&lt;/span&gt;"></div></div>



<div class="project-show-files">
<div class="tree-holder clearfix" id="tree-holder">
<div class="nav-block">
<div class="tree-ref-container">
<div class="tree-ref-holder">
<form class="project-refs-form" action="/BotolBaba/bind/-/refs/switch" accept-charset="UTF-8" method="get"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="destination" id="destination" value="tree" />
<input type="hidden" name="path" id="path" value="" />
<div class="dropdown">
<button class="dropdown-menu-toggle js-project-refs-dropdown qa-branches-select" type="button" data-toggle="dropdown" data-selected="master" data-ref="master" data-refs-url="/BotolBaba/bind/refs?sort=updated_desc" data-field-name="ref" data-submit-form-on-click="true" data-visit="true"><span class="dropdown-toggle-text ">master</span><i aria-hidden="true" data-hidden="true" class="fa fa-chevron-down"></i></button>
<div class="dropdown-menu dropdown-menu-paging dropdown-menu-selectable git-revision-dropdown qa-branches-dropdown">
<div class="dropdown-page-one">
<div class="dropdown-title"><span>Switch branch/tag</span><button class="dropdown-title-button dropdown-menu-close" aria-label="Close" type="button"><i aria-hidden="true" data-hidden="true" class="fa fa-times dropdown-menu-close-icon"></i></button></div>
<div class="dropdown-input"><input type="search" id="" class="dropdown-input-field qa-dropdown-input-field" placeholder="Search branches and tags" autocomplete="off" /><i aria-hidden="true" data-hidden="true" class="fa fa-search dropdown-input-search"></i><i aria-hidden="true" data-hidden="true" role="button" class="fa fa-times dropdown-input-clear js-dropdown-input-clear"></i></div>
<div class="dropdown-content"></div>
<div class="dropdown-loading"><i aria-hidden="true" data-hidden="true" class="fa fa-spinner fa-spin"></i></div>
</div>
</div>
</div>
</form>
</div>
<div data-can-collaborate="false" data-can-edit-tree="false" data-fork-new-blob-path="/BotolBaba/bind/-/forks?continue%5Bnotice%5D=You%27re+not+allowed+to+make+changes+to+this+project+directly.+A+fork+of+this+project+has+been+created+that+you+can+make+changes+in%2C+so+you+can+submit+a+merge+request.&amp;continue%5Bnotice_now%5D=You%27re+not+allowed+to+make+changes+to+this+project+directly.+A+fork+of+this+project+is+being+created+that+you+can+make+changes+in%2C+so+you+can+submit+a+merge+request.&amp;continue%5Bto%5D=%2FBotolBaba%2Fbind%2F-%2Fnew%2Fmaster&amp;namespace_key=9238839" data-fork-new-directory-path="/BotolBaba/bind/-/forks?continue%5Bnotice%5D=You%27re+not+allowed+to+make+changes+to+this+project+directly.+A+fork+of+this+project+has+been+created+that+you+can+make+changes+in%2C+so+you+can+submit+a+merge+request.+Try+to+create+a+new+directory+again.&amp;continue%5Bnotice_now%5D=You%27re+not+allowed+to+make+changes+to+this+project+directly.+A+fork+of+this+project+is+being+created+that+you+can+make+changes+in%2C+so+you+can+submit+a+merge+request.&amp;continue%5Bto%5D=%2FBotolBaba%2Fbind&amp;namespace_key=9238839" data-fork-upload-blob-path="/BotolBaba/bind/-/forks?continue%5Bnotice%5D=You%27re+not+allowed+to+make+changes+to+this+project+directly.+A+fork+of+this+project+has+been+created+that+you+can+make+changes+in%2C+so+you+can+submit+a+merge+request.+Try+to+upload+a+file+again.&amp;continue%5Bnotice_now%5D=You%27re+not+allowed+to+make+changes+to+this+project+directly.+A+fork+of+this+project+is+being+created+that+you+can+make+changes+in%2C+so+you+can+submit+a+merge+request.&amp;continue%5Bto%5D=%2FBotolBaba%2Fbind&amp;namespace_key=9238839" data-new-blob-path="/BotolBaba/bind/-/new/master" data-new-branch-path="/BotolBaba/bind/-/branches/new" data-new-dir-path="/BotolBaba/bind/-/create_dir/master" data-new-tag-path="/BotolBaba/bind/-/tags/new" data-upload-path="/BotolBaba/bind/-/create/master" id="js-repo-breadcrumb"></div>
</div>
<div class="tree-controls">
<div class="d-block d-sm-flex flex-wrap align-items-start gl-children-ml-sm-3"><span class="btn path-lock js-path-lock js-hide-on-root hidden disabled has-tooltip" data-toggle="tooltip" title="You do not have permission to lock this" data-qa-selector="disabled_lock_button">Lock</span>
<div class="d-inline-block" data-history-link="/BotolBaba/bind/-/commits/master" id="js-tree-history-link"></div>
<a class="btn shortcuts-find-file" rel="nofollow" href="/BotolBaba/bind/-/find_file/master">Find file
</a><a class="btn btn-default qa-web-ide-button" data-target="#modal-confirm-fork" data-toggle="modal" href="#modal-confirm-fork">Web IDE</a><div class="modal" data-qa-selector="confirm_fork_modal" id="modal-confirm-fork">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h3 class="page-title">Fork project?</h3>
<button aria-label="Close" class="close" data-dismiss="modal" type="button">
<span aria-hidden>&times;</span>
</button>
</div>
<div class="modal-body p-3">
<p>You&#39;re not allowed to edit files in this project directly. Please fork this project, make your changes there, and submit a merge request.</p>
</div>
<div class="modal-footer">
<a class="btn btn-cancel" data-dismiss="modal" href="#">Cancel</a>
<a class="btn btn-success" data-qa-selector="fork_project_button" rel="nofollow" data-method="post" href="/BotolBaba/bind/-/forks?continue%5Bnotice%5D=You%27re+not+allowed+to+make+changes+to+this+project+directly.+A+fork+of+this+project+has+been+created+that+you+can+make+changes+in%2C+so+you+can+submit+a+merge+request.&amp;continue%5Bnotice_now%5D=You%27re+not+allowed+to+make+changes+to+this+project+directly.+A+fork+of+this+project+is+being+created+that+you+can+make+changes+in%2C+so+you+can+submit+a+merge+request.&amp;continue%5Bto%5D=%2F-%2Fide%2Fproject%2FD4RK-PH1N1X14N-Mr.-SS7%2Fbind%2Fedit%2Fmaster&amp;namespace_key=9238839">Fork project</a>
</div>
</div>
</div>
</div>
<div class="project-action-button dropdown inline">
<button aria-label="Download" class="btn has-tooltip" data-qa-selector="download_source_code_button" data-display="static" data-toggle="dropdown" title="Download">
<svg class="s16" data-testid="download-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#download"></use></svg>
<span class="sr-only">Select Archive Format</span>
<svg class="s16" data-testid="chevron-down-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#chevron-down"></use></svg>
</button>
<div class="dropdown-menu dropdown-menu-right" role="menu">
<section>
<h5 class="m-0 dropdown-bold-header">Download source code</h5>
<div class="dropdown-menu-content">
<div class="btn-group ml-0 w-100">
<a rel="nofollow" download="" class="btn btn-xs btn-primary" href="/BotolBaba/bind/-/archive/master/bind-master.zip">zip</a>
<a rel="nofollow" download="" class="btn btn-xs " href="/BotolBaba/bind/-/archive/master/bind-master.tar.gz">tar.gz</a>
<a rel="nofollow" download="" class="btn btn-xs " href="/BotolBaba/bind/-/archive/master/bind-master.tar.bz2">tar.bz2</a>
<a rel="nofollow" download="" class="btn btn-xs " href="/BotolBaba/bind/-/archive/master/bind-master.tar">tar</a>
</div>

</div>
</section>
<div data-links="[{&quot;text&quot;:&quot;zip&quot;,&quot;path&quot;:&quot;/BotolBaba/bind/-/archive/master/bind-master.zip&quot;},{&quot;text&quot;:&quot;tar.gz&quot;,&quot;path&quot;:&quot;/BotolBaba/bind/-/archive/master/bind-master.tar.gz&quot;},{&quot;text&quot;:&quot;tar.bz2&quot;,&quot;path&quot;:&quot;/BotolBaba/bind/-/archive/master/bind-master.tar.bz2&quot;},{&quot;text&quot;:&quot;tar&quot;,&quot;path&quot;:&quot;/BotolBaba/bind/-/archive/master/bind-master.tar&quot;}]" id="js-directory-downloads"></div>
</div>
</div><div class="project-clone-holder d-none d-md-inline-block">
<div class="git-clone-holder js-git-clone-holder">
<a class="btn btn-primary clone-dropdown-btn qa-clone-dropdown" data-toggle="dropdown" href="#" id="clone-dropdown">
<span class="gl-mr-2 js-clone-dropdown-label">
Clone
</span>
<svg class="s16 icon" data-testid="chevron-down-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#chevron-down"></use></svg>
</a>
<ul class="clone-options-dropdown dropdown-menu dropdown-menu-large dropdown-menu-right dropdown-menu-selectable p-3 qa-clone-options">
<li>
<label class="label-bold">
Clone with SSH
</label>
<div class="input-group">
<input type="text" name="ssh_project_clone" id="ssh_project_clone" value="git@gitlab.com:BotolBaba/bind.git" class="js-select-on-focus form-control qa-ssh-clone-url" readonly="readonly" aria-label="Project clone URL" />
<div class="input-group-append">
<button class="btn input-group-text btn-default btn-clipboard" data-toggle="tooltip" data-placement="bottom" data-container="body" data-title="Copy URL" data-class="input-group-text btn-default btn-clipboard" data-clipboard-target="#ssh_project_clone" type="button" title="Copy URL" aria-label="Copy URL"><svg class="s16" data-testid="copy-to-clipboard-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#copy-to-clipboard"></use></svg></button>

</div>
</div>
</li>
<li class="pt-2">
<label class="label-bold">
Clone with HTTPS
</label>
<div class="input-group">
<input type="text" name="http_project_clone" id="http_project_clone" value="https://gitlab.com/BotolBaba/bind.git" class="js-select-on-focus form-control qa-http-clone-url" readonly="readonly" aria-label="Project clone URL" />
<div class="input-group-append">
<button class="btn input-group-text btn-default btn-clipboard" data-toggle="tooltip" data-placement="bottom" data-container="body" data-title="Copy URL" data-class="input-group-text btn-default btn-clipboard" data-clipboard-target="#http_project_clone" type="button" title="Copy URL" aria-label="Copy URL"><svg class="s16" data-testid="copy-to-clipboard-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#copy-to-clipboard"></use></svg></button>

</div>
</div>
</li>

</ul>
</div>

</div></div><div class="project-clone-holder d-block d-md-none mt-sm-2 mt-md-0 ml-sm-2">
<div class="btn-group mobile-git-clone js-mobile-git-clone btn-block">
<button class="btn btn-primary flex-fill bold justify-content-center input-group-text clone-dropdown-btn js-clone-dropdown-label" data-toggle="tooltip" data-placement="bottom" data-container="body" data-button-text="Copy HTTPS clone URL" data-hide-button-icon="true" data-class="btn-primary flex-fill bold justify-content-center input-group-text clone-dropdown-btn js-clone-dropdown-label" data-clipboard-text="https://gitlab.com/BotolBaba/bind.git" type="button" title="Copy" aria-label="Copy">Copy HTTPS clone URL</button>
<button class="btn btn-primary dropdown-toggle js-dropdown-toggle flex-grow-0 d-flex-center w-auto ml-0" data-toggle="dropdown" type="button">
<svg class="s16 dropdown-btn-icon icon" data-testid="chevron-down-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#chevron-down"></use></svg>
</button>
<ul class="dropdown-menu dropdown-menu-selectable dropdown-menu-right clone-options-dropdown" data-dropdown>
<li>
<a class="copy ssh clone url-selector is-active" href="git@gitlab.com:BotolBaba/bind.git" data-clone-type="ssh"><strong class="dropdown-menu-inner-title">Copy SSH clone URL</strong><span class="dropdown-menu-inner-content"><span class="__cf_email__" data-cfemail="73141a0733141a071f12115d101c1e">[email&#160;protected]</span>:BotolBaba/bind.git</span></a>
</li>
<li>
<a class="copy https clone url-selector " href="https://gitlab.com/BotolBaba/bind.git" data-clone-type="http"><strong class="dropdown-menu-inner-title">Copy HTTPS clone URL</strong><span class="dropdown-menu-inner-content">https://gitlab.com/BotolBaba/bind.git</span></a>
</li>

</ul>
</div>

</div></div>

</div>
<div id="js-last-commit"></div>
<div class="project-buttons gl-mb-3 js-show-on-project-root">
<ul class="nav">
<li class="nav-item">
<a class="nav-link btn btn-default d-flex align-items-center" href="/BotolBaba/bind/-/blob/master/README.md"><svg class="s16 icon gl-mr-2" data-testid="doc-text-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#doc-text"></use></svg>README</a></li>
<li class="nav-item">
<div class="stat-text d-flex align-items-center"><svg class="s16 icon gl-mr-2" data-testid="scale-icon"><use xlink:href="https://gitlab.com/assets/icons-56afbe04f1544b1e0d40a3efbc29d75dfd2357ee5ce4f5c1ca171d6cf589fe12.svg#scale"></use></svg><span class="project-stat-value">No license. All rights reserved</span></div>
</li>
</ul>

</div>
<div data-escaped-ref="master" data-full-name="Mehedi Hasan Ariyan / bind" data-path-locks-available="true" data-path-locks-toggle="/BotolBaba/bind/path_locks/toggle" data-project-path="BotolBaba/bind" data-project-short-path="bind" data-ref="master" id="js-tree-list"></div>
</div>

</div>
</div>

</div>
</div>
</div>
</div>


<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script nonce="Vqh9ZOuKQQXxBS8LR2PXfA==">
//<![CDATA[
if ('loading' in HTMLImageElement.prototype) {
  document.querySelectorAll('img.lazy').forEach(img => {
    img.loading = 'lazy';
    let imgUrl = img.dataset.src;
    // Only adding width + height for avatars for now
    if (imgUrl.indexOf('/avatar/') > -1 && imgUrl.indexOf('?') === -1) {
      const targetWidth = img.getAttribute('width') || img.width;
      imgUrl += `?width=${targetWidth}`;
    }
    img.src = imgUrl;
    img.removeAttribute('data-src');
    img.classList.remove('lazy');
    img.classList.add('js-lazy-loaded', 'qa-js-lazy-loaded');
  });
}

//]]>
</script>
<div aria-labelledby="custom-notifications-title" class="modal fade" id="modal-6974946-21003539-Project" role="dialog" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="custom-notifications-title">
Custom notification events
</h4>
<button aria-label="Close" class="close" data-dismiss="modal" type="button">
<span aria-hidden>&times;</span>
</button>
</div>
<div class="modal-body">
<div class="container-fluid">
<form class="custom-notifications-form" id="new_notification_setting" action="/-/notification_settings" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="authenticity_token" value="otiIFMbv4XfYXgWkzKaNYAlFANJU1uWw8g0VoJOmUwMJPwyYTaSBdZB6KISob84PTz6iGpV9+kPZaM//uCB0ng==" /><input type="hidden" name="project_id" id="project_id" value="21003539" />
<input type="hidden" name="hide_label" id="hide_label" value="true" />
<div class="row">
<div class="col-lg-4">
<h4 class="gl-mt-0">Notification events</h4>
<p>
Custom notification levels are the same as participating levels. With custom notification levels you will also receive notifications for select events. To find out more, check out <a target="_blank" href="/help/user/profile/notifications">notification emails</a>.
</p>
</div>
<div class="col-lg-8">
<div class="form-group">
<div class="form-check gl-mt-0">
<input name="notification_setting[new_release]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[new_release]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[new_release]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[new_release]">
<strong>
New release
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[new_note]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[new_note]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[new_note]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[new_note]">
<strong>
New note
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[new_issue]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[new_issue]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[new_issue]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[new_issue]">
<strong>
New issue
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[reopen_issue]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[reopen_issue]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[reopen_issue]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[reopen_issue]">
<strong>
Reopen issue
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[close_issue]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[close_issue]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[close_issue]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[close_issue]">
<strong>
Close issue
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[reassign_issue]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[reassign_issue]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[reassign_issue]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[reassign_issue]">
<strong>
Reassign issue
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[issue_due]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[issue_due]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[issue_due]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[issue_due]">
<strong>
Issue due
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[new_merge_request]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[new_merge_request]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[new_merge_request]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[new_merge_request]">
<strong>
New merge request
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[push_to_merge_request]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[push_to_merge_request]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[push_to_merge_request]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[push_to_merge_request]">
<strong>
Push to merge request
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[reopen_merge_request]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[reopen_merge_request]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[reopen_merge_request]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[reopen_merge_request]">
<strong>
Reopen merge request
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[close_merge_request]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[close_merge_request]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[close_merge_request]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[close_merge_request]">
<strong>
Close merge request
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[reassign_merge_request]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[reassign_merge_request]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[reassign_merge_request]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[reassign_merge_request]">
<strong>
Reassign merge request
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[merge_merge_request]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[merge_merge_request]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[merge_merge_request]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[merge_merge_request]">
<strong>
Merge merge request
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[failed_pipeline]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[failed_pipeline]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" checked="checked" name="notification_setting[failed_pipeline]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[failed_pipeline]">
<strong>
Failed pipeline
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[fixed_pipeline]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[fixed_pipeline]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" checked="checked" name="notification_setting[fixed_pipeline]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[fixed_pipeline]">
<strong>
Fixed pipeline
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[success_pipeline]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[success_pipeline]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" name="notification_setting[success_pipeline]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[success_pipeline]">
<strong>
Successful pipeline
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
<div class="form-group">
<div class="form-check">
<input name="notification_setting[moved_project]" type="hidden" value="0" /><input id="modal-6974946-21003539-Project_notification_setting[moved_project]" class="js-custom-notification-event form-check-input" type="checkbox" value="1" checked="checked" name="notification_setting[moved_project]" />
<label class="form-check-label" for="modal-6974946-21003539-Project_notification_setting[moved_project]">
<strong>
Moved project
<div class="fa custom-notification-event-loading spinner"></div>
</strong>
</label>
</div>
</div>
</div>
</div>
</form></div>
</div>
</div>
</div>
</div>


</body>
</html>

